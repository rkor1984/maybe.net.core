﻿using System;
using System.Collections.Generic;
using System.Linq;
using Maybe;
using Shouldly;
using Xunit;

namespace MayBeTests.UsageTests
{
    public class MaybeTests
    {
        public class AccountRepository
        {
            readonly List<Account> _accounts = new List<Account>
            {
                new Account
                {
                    Name = "Rupi"
                }
            };

            public Maybe<Account> FindUsingToMaybe(string name)
            {
                var account = _accounts.FirstOrDefault(x => x.Name == name);
                return account.ToMaybe();
            }
            public Maybe<Account> FindUsingSomeOrNone(string name)
            {
                var account = _accounts.FirstOrDefault(x => x.Name == name);
                return account != null ? Maybe<Account>.Some(account) : Maybe<Account>.None;
            }

            internal object NoneIfEmpty(string v)
            {
                throw new NotImplementedException();
            }
        }

        public class Account
        {
            public string Name { get; set; }

            public override string ToString()
            {
                return "Account for " + Name;
            }
        }

        [Fact]
        public void ToMaybeTest()
        {
            var rep = new AccountRepository();
            var account = rep.FindUsingToMaybe("Rupi");
            account.HasValue.ShouldBe(true);
            account.Value.Name.ShouldBe("Rupi");
            var account2 = rep.FindUsingToMaybe("r");
            account2.HasValue.ShouldBe(false);
        }

        [Fact]
        public void SomeOrNoneTest()
        {
            var rep = new AccountRepository();
            var account = rep.FindUsingSomeOrNone("Rupi");
            account.HasValue.ShouldBe(true);
            account.Value.Name.ShouldBe("Rupi");
            var account2 = rep.FindUsingSomeOrNone("r");
            account2.HasValue.ShouldBe(false);
        }

        [Fact]
        public void ComparisonTest()
        {
            var rep = new AccountRepository();
            var account = rep.FindUsingToMaybe("Rupi");

            account.HasValue.ShouldBe(true);
            account.Value.Name.ShouldBe("Rupi");

            var account2 = rep.FindUsingSomeOrNone("Rupi");

            account2.HasValue.ShouldBe(true);
            account2.Value.Name.ShouldBe("Rupi");

            (account2.Equals(account)).ShouldBe(true);
            (account2 != account).ShouldBe(false);
            (account2 == account).ShouldBe(true);
        }

        [Fact]
        public void ValueOrThrowShouldThrowExceptionIfValueIsNull()
        {
            var rep = new AccountRepository();
            var account = rep.FindUsingSomeOrNone("r");
            Assert.Throws<Exception>(() => account.ValueOrThrow(new Exception("ValueOrThrow thrown an Exception")));
        }

        [Fact]
        public void ValueOrDefaultShouldReturnDefaultValueIfNull()
        {
            var rep = new AccountRepository();
            var account = rep.FindUsingSomeOrNone("r");
            account.ValueOrDefault(new Account
            {
                Name = "rupi"
            }).Name.ShouldBe("rupi");
            account.ValueOrDefault(null).ShouldBe(null);
            account.HasValue.ShouldBe(false);
        }

        [Fact]
        public void ToStringShouldReturnValueifPresent()
        {
            var rep = new AccountRepository();
            var account = rep.FindUsingSomeOrNone("Rupi");
            account.HasValue.ShouldBe(true);
            account.ToString().ShouldBe("Account for Rupi");
        }
    }
}
