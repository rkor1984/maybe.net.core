﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Maybe
{
    public struct Maybe<T> : IEquatable<Maybe<T>>
    {
        readonly IEnumerable<T> _values;

        public static Maybe<T> Some(T value)
        {
            if (value == null)
            {
                throw new InvalidOperationException();
            }

            return new Maybe<T>(new[] { value });
        }

        public static Maybe<T> None => new Maybe<T>(new T[0]);

        private Maybe(IEnumerable<T> values)
        {
            this._values = values;
        }

        public bool HasValue => _values != null && _values.Any();

        public T Value
        {
            get
            {
                if (!HasValue)
                {
                    throw new InvalidOperationException("Maybe does not have a value");
                }

                return _values.Single();
            }
        }

        public T ValueOrDefault(T @default)
        {
            if (!HasValue)
            {
                return @default;
            }
            return _values.Single();
        }

        public T ValueOrThrow(Exception e)
        {
            if (HasValue)
            {
                return Value;
            }
            throw e;
        }

        public bool Equals(Maybe<T> other)
        {
            return EqualityComparer<T>.Default.Equals(Value, other.Value) && HasValue.Equals(other.HasValue);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Maybe<T> && Equals((Maybe<T>)obj);
        }
        public static bool operator ==(Maybe<T> left, Maybe<T> right)
        {
            return left.Equals(right);
        }
        public static bool operator !=(Maybe<T> left, Maybe<T> right)
        {
            return !left.Equals(right);
        }

        public U Case<U>(Func<T, U> some, Func<U> none)
        {
            return HasValue
                ? some(Value)
                : none();
        }

        public void Case(Action<T> some, Action none)
        {
            if (HasValue)
            {
                some(Value);
            }
            else
            {
                none();
            }
        }
        public Maybe<U> Map<U>(Func<T, Maybe<U>> map)
        {
            return HasValue
                ? map(Value)
                : Maybe<U>.None;
        }

        public Maybe<U> Map<U>(Func<T, U> map)
        {
            return HasValue
                ? Maybe.Some(map(Value))
                : Maybe<U>.None;
        }
        /// <inheritdoc />
        public override string ToString()
        {
            if (!HasValue)
            {
                return "<Nothing>";
            }

            return Value.ToString();
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (EqualityComparer<T>.Default.GetHashCode(Value) * 397) ^ HasValue.GetHashCode();
            }
        }
    }

    public static class Maybe
    {
        public static Maybe<T> Some<T>(T value)
        {
            return Maybe<T>.Some(value);
        }
    }
}
